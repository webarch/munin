# Webarchitects Munin Ansible Role

This role can be used to configure Munin servers, hosts in a `munin_masters` inventory group and Munin Nodes, hosts in a `munin_nodes` inventory group.

## Munin Master

The Apache configuration for Munin master servers can be generated using the [users role](https://git.coop/webarch/users) and a `munin` user dictionary:

```yaml
users:
  munin:
    users_state: present
    users_name: munin application user
    users_system: true
    users_shell: /bin/false
    users_apache_cgi_extension_match: false
    users_apache_virtual_hosts:
      redirect:
        users_apache_server_name: "www.{{ inventory_hostname }}"
        users_apache_redirects:
          - regex_path: (.*)
            url: "https://{{ inventory_hostname }}"
        users_apache_vhost_docroot: false
      munin:
        users_apache_type: fcgi
        users_apache_vhost_docroot: false
        users_apache_server_name: "{{ inventory_hostname }}"
        users_apache_robots: deny
        users_apache_locations:
          - location: /
            authname: Munin
        users_apache_htauth_users:
          - name: munin
            password: "{{ vault_munin_password }}"
        users_apache_alias:
          - script: /munin-cgi/munin-cgi-graph
            path: /usr/lib/munin/cgi/munin-cgi-graph
          - script: /munin-cgi/munin-cgi-html
            path: /usr/lib/munin/cgi/munin-cgi-html
          - url: /munin/static
            path: /var/cache/munin/www/static
          - script: /munin
            path: /usr/lib/munin/cgi/munin-cgi-html
        users_apache_directories:
          "/var/cache/munin/www":
            users_apache_docroot: true
            users_apache_options:
              - +Indexes
              - -ExecCGI
            users_apache_expires: active
          "/usr/lib/munin/cgi":
            users_apache_type: fcgi
            users_apache_options:
              - +ExecCGI
              - -MultiViews
              - +SymLinksIfOwnerMatch
            users_apache_ssl_options:
              - +StdEnvVars
        users_apache_redirects:
          - regex_path: ^/$
            url: "https://{{ inventory_hostname }}/munin/"
    users_home: /var/lib/munin
    users_home_group: munin
    users_home_owner: munin
    users_home_mode: "0755"
    users_group_members:
      - www-data
```
